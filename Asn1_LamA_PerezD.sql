--This sets the script to output on the cmd
SET ECHO ON
--This spools the output to a text file
SPOOL D:\Nextcloud\Documents\BCIT\full-time\2714\Assignment\assgn1\asn1_LamA_PerezD.txt
--1.Dropping the tables first
DROP TABLE Hotel;
DROP TABLE Room;
DROP TABLE Booking;
DROP TABLE Guest;
DROP TABLE OldBooking;
--2.Create Hotel and room
CREATE TABLE Hotel
(hotelNo  CHAR(8)     NOT NULL
,hoteName VARCHAR(30) NOT NULL
,city     VARCHAR(30) NOT NULL
,CONSTRAINT PKHotel PRIMARY KEY(hotelNo)
);
--
CREATE TABLE Room
(roomNo   CHAR(8)     NOT NULL
,hotelNo  CHAR(8)     NOT NULL
,roomType VARCHAR(30) NOT NULL
,price    CHAR(8)     NOT NULL
,discount CHAR(3)     DEFAULT 0 NOT NULL
,CONSTRAINT PKRoom PRIMARY KEY(roomNo)
,CONSTRAINT FKhotelNo FOREIGN KEY (hotelNo)
                      REFERENCES Hotel (hotelNo)
,CONSTRAINT chk_roomType
	CHECK(roomType IN('Single','Double', 'Family','Deluxe'))
,CONSTRAINT chk_price
	CHECK(price >= 10 and price <=100)
,CONSTRAINT chk_roomNo
	CHECK(roomNo >= 1 and roomNo <= 100)
,CONSTRAINT chk_discount
	CHECK(discount >= 0 and discount <= 30)
);
--3.Create Booking and Guest
CREATE TABLE Guest
(guestNo      CHAR(8)     NOT NULL
,guestName    VARCHAR(30) NOT NULL
,guestAddress VARCHAR(30) NOT NULL
,CONSTRAINT PKGuest PRIMARY KEY(guestNo)
);
--
CREATE TABLE Booking
(hotelNo  CHAR(8) NOT NULL
,guestNo  CHAR(8) NOT NULL
,dateFrom DATE    NOT NULL
,dateTo   DATE    NOT NULL
,roomNo   CHAR(8) NOT NULL
,CONSTRAINT PKBooking PRIMARY KEY(dateFrom)
,CONSTRAINT FKhotelNo FOREIGN KEY (hotelNo)
                      REFERENCES Hotel (hotelNo)
,CONSTRAINT FKguestNo FOREIGN KEY (guestNo)
                      REFERENCES Guest (guestNo)
);
--4.Insert 3 rows of sample data in each of the four tables DONE
INSERT INTO Hotel (hotelNo, hotelName, city)
	VALUES(4586, 'Merlot', 'Richmond')
	     ,(4586, 'Merlot', 'Richmond')
	     ,(4586, 'Merlot', 'Richmond');
INSERT INTO Room (roomNo, hotelNo, roomType, price)
	VALUES(557, 4586, 'single', 45)
	     ,(557, 4586, 'single', 45)
	     ,(557, 4586, 'single', 45);
INSERT INTO Booking (hotelNo, guestNo, dateFrom, dateTo, roomNo)
	VALUES(4586, 234, '2016-12-07', '2016-12-07', 456)
	     ,(4586, 234, '2016-12-07', '2016-12-07', 456)
	     ,(4586, 234, '2016-12-07', '2016-12-07', 456);
INSERT INTO Guest (guestNo, guestName, guestAddress)
	VALUES(234, 'John Smith', '3495 No.7 Road')
	     ,(234, 'John Smith', '3495 No.7 Road')
	     ,(234, 'John Smith', '3495 No.7 Road');
--5.Adding in the 4th room type of 'Deluxe', add discount column to Room DONE
--DO 6 AND 7
--6.One hotel must increase price of Double by 15%, modifying one booking for guest 
--7.Create an OldBooking table for archiving
CREATE TABLE OldBooking
(
);
SPOOL OFF
